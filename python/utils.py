from time import sleep
from random import random

def getCaptcha(browser, url):
    browser.get(url)
    sleep(0.5+random())
    while len(browser.find_elements_by_tag_name("svg")) == 0:
        sleep(5)

def printLoading(outOfTen, action):
    print("[{}] - {}".format("="*outOfTen+" "*(10-outOfTen), action+" "*25), end="\r")