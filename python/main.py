from selenium import webdriver, common
from selenium_stealth import stealth
from selenium.webdriver.common.by import By
from random import *
from settings import *
from utils import *
import tkinter.messagebox
import json

variables = set()
dataTable = []

url = "https://www.leboncoin.fr/recherche?category=10&locations=Montrouge_92120__48.81847_2.3198_1629_5000&real_estate_type=2&price=min-1700&square=20-90&page="

option = webdriver.ChromeOptions()
option.add_argument("--incognito")
option.add_argument("referer=https://ecosia.fr")
option.add_experimental_option("excludeSwitches", ["enable-automation"])
option.add_experimental_option('useAutomationExtension', False)

browserPages = webdriver.Chrome(executable_path=CHROME_DRIVER_PATH, chrome_options=option)
browserAds = webdriver.Chrome(executable_path=CHROME_DRIVER_PATH, chrome_options=option)
browserAds.implicitly_wait(5)

stealth(browserPages,
        languages=["fr-FR", "fr", "en-US", "en"],
        vendor="Google Inc.",
        platform="Win32",
        webgl_vendor="Intel Inc.",
        renderer="Intel Iris OpenGL Engine",
        fix_hairline=True,
        )

stealth(browserAds,
        languages=["fr-FR", "fr", "en-US", "en"],
        vendor="Google Inc.",
        platform="Win32",
        webgl_vendor="Intel Inc.",
        renderer="Intel Iris OpenGL Engine",
        fix_hairline=True,
        )

xpath_pool = [("title", "//h1[@data-qa-id='adview_title']"), ("date", "//p[@data-qa-id='adview_date']")]

test = True

# for i in range(1,2):
for i in range(1,101):
    print("Page : "+str(i))
    getCaptcha(browserPages, url + str(i))
    adCards = browserPages.find_elements_by_xpath("//a[@data-qa-id='aditem_container']")
    sleep(2+random())
    for adUrl in [adCard.get_attribute("href") for adCard in adCards]:
        adUrlWoAc = adUrl.split("?")[0] + "?ac=" + str(randint(100000000, 999999999))
        dataLine = {}
        dataLine["url"] = adUrlWoAc
        doWhile = True
        printLoading(0, "Getting page")
        getCaptcha(browserAds, adUrlWoAc)
        while doWhile:
            printLoading(1, "Loop beginning")
            doWhile = False
            if len(browserAds.find_elements_by_css_selector("div.geetest_radar_btn")):
                printLoading(2, "Waiting for Captcha resolve")
                tkinter.messagebox.showinfo('Captcha !','Il y a un captcha détecté')
            xpath_pool_randomized = xpath_pool[:]
            shuffle(xpath_pool_randomized)
            for attr, xpath in xpath_pool_randomized:
                dataLine[attr] = browserAds.find_element_by_xpath(xpath).text

            printLoading(3, "Getting address")
            dataLine["addresse"] = browserAds.find_element_by_css_selector("\
                div._3JOrc._1akwr._2liDR._1mwQl.aj3_W.FB92D._3qvnf.PYBnx.rvrtO._23jKN._2hbpZ._1_1QY._1qvhT \
                h2.Roh2X._3c6yv._25dUs._21rqc._3QJkO._1hnil._1-TTU._35DXM").text

            printLoading(4, "First Waiting")
            sleep(3+random())
            printLoading(5, "Getting button 'Voir plus'")
            button_more = browserAds.find_elements_by_xpath("//button[@data-qa-id='criteria_more']")
            if len(button_more):
                browserAds.execute_script("arguments[0].click();", button_more[0])

            printLoading(6, "Finding criteria")
            criteria_div = browserAds.find_elements_by_xpath("//div[@data-qa-id='criteria_container']/div")
            try :
                for i in range(0, len(criteria_div)):
                    printLoading(7, "Getting attribut")
                    attr = ""
                    title = criteria_div[i].find_elements_by_tag_name("p")
                    if len(title):
                        attr = title[0].text
                    else:
                        attr = criteria_div[i].find_element_by_css_selector("._2k43C._1pHkp._21rqc._3QJkO.Dqdzf.cJtdT._3j0OU").text.split('\n', 1)[0]
                    if attr == '':
                        raise common.exceptions.NoSuchElementException

                    printLoading(8, "Getting value")
                    value = criteria_div[i].find_elements_by_tag_name("span")
                    if len(value):
                        dataLine[attr] = value[0].text
                    else:
                        dataLine[attr] = criteria_div[i].find_element_by_css_selector("div.styles_active__j3Tua").text
                    if dataLine[attr] == '':
                        raise common.exceptions.NoSuchElementException

            except common.exceptions.StaleElementReferenceException:
                printLoading(9, "EXCEPTION !!!")
                sleep(1+random())
                doWhile = True
            except common.exceptions.NoSuchElementException:
                printLoading(9, "EXCEPTION !!!")
                sleep(1+random())
                doWhile = True
            printLoading(10, "Final sleep")
            sleep(4+random())
        dataTable.append(dataLine)
        print("[=========")
        print(dataLine)

print(dataTable)

with open('Test4.json', 'w') as f:
    json.dump(dataTable, f, ensure_ascii=False)

browserPages.quit()
browserAds.quit()