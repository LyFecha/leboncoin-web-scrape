## Robot de Web scraping en Python.

ATTENTION : ce robot est incomplet et ne scrape pas le prix ni le caractère urgent, à la une, ...

### Initialisation

Installer les paquets requis :
```python
pip install -r requirements.txt
```
Vous pouvez aussi utiliser un environement virtuel avec le package venv.

Il vous faudra aussi la [dernière version du driver de chrome](https://chromedriver.chromium.org/downloads) si vous utilisez Chrome (si vous utilisez Firefox ou autre, il faudra télécharger le driver + faire des modifications de code)

### Lancement

Dans le dossier "/python" du projet dans le terminal :
```python
python main.py
```

### A savoir

Les varibles qui pourraient vous intéressées sont :
- "url", url coorespondant à la page de recherche où l'on met des filtres de recherche qui doit finir par "&page=" pour pouvoir parcourir les pages d'annonces.
- "f", fichier de sortie (à la fin)
- les valeurs des "sleep", qui peuvent être trop long ou court pour vous.
