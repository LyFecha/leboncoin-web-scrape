## Algorithme de Web Scraping pour le site LeBonCoin.fr

Vous pourrez trouver dans ce projet deux robot de web scraping :

- un en [JavaScript](https://gitlab.com/LyFecha/leboncoin-web-scrape/-/tree/master/javascript) (recommandé : il est plus discret et plus rapide)
- un en [Python](https://gitlab.com/LyFecha/leboncoin-web-scrape/-/tree/master/python) (non recommandé, mais il est peut-être plus facile à comprendre)
