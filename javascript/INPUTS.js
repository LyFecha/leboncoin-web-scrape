const URL = "https://www.leboncoin.fr/recherche?category=10&locations=Montrouge_92120__48.81847_2.3198_1629_5000&real_estate_type=2&price=min-1700&square=20-90"
const NUMBER_OF_PAGES = 100
const OUTPUT_NAME = "FinalOutput" // Don't put the file extension
const OUTPUT_TYPE = "csv" // "json" or "csv" or "both". Default : "json"

module.exports = { URL, NUMBER_OF_PAGES, OUTPUT_NAME, OUTPUT_TYPE }