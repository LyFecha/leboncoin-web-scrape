## Robot de Web scraping en JavaScript (NodeJS).

### Initialisation

Installer les paquets requis :
```
npm i
```
ou si vous avez yarn
```
yarn install
```

Il n'y a pas besoin de driver pour navigateur, Puppeteer (module utilisé pour le web scraping) en comptient un de chromium.

## Lancement

Dans le dossier "/JavaScript" du projet dans le terminal :
```
node main.js
```

## A savoir

Les varibles qui pourraient vous intéressées sont toutes dans INPUTS.js :
- URL : url coorespondant à la page de recherche où l'on met des filtres de recherche. Contrairement à python, ne doit pas contenir "&page=" à la fin !
- NUMBER_OF_PAGES : nombre de pages (pas nombre d'annonces) à scraper
- OUTPUT_NAME : nom du fichier en sortie qui sera dans "/output" sans l'extension de fichier
- OUTPUT_TYPE : format du fichier de sortie : .csv, .json ou les deux.
