const puppeteer = require('puppeteer-extra');
const fs = require('fs');
const ObjectsToCsv = require('objects-to-csv');
const readline = require('readline');
const { URL, NUMBER_OF_PAGES, OUTPUT_NAME, OUTPUT_TYPE } = require('./INPUTS');

const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())


main = async (browser) => {

  const page = await browser.newPage()
  await page.setDefaultTimeout(3000)
  await page.setUserAgent('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36');

  dataTable = []

  for (let i = 1; i <= NUMBER_OF_PAGES; i++) {
    console.log(`Page : ${i}`)
    await getCaptcha(page, URL + `&page=${i}`)
    await page.waitForXPath("//a[@data-qa-id='aditem_container']")

    adUrls = await page.$$eval("a[data-qa-id='aditem_container']", (adCards) => adCards.map((adCard) => adCard.href))
    for (adUrl of adUrls) {
      console.log(adUrl)
      adUrlAc = `${adUrl.split("?", 1)[0]}?ac=${Math.floor(
        Math.random() * (999999999 - 100000000 + 1) + 100000000
      )}`
      const dataLine = {}
      dataLine["url"] = adUrlAc
      let doWhile = true
      printLoading(0, "Getting page")
      await getCaptcha(page, adUrlAc)
      await page.setDefaultTimeout(1000)
      while (doWhile) {
        printLoading(1, "Loop beginning")
        doWhile = false
        while (!(await page.waitForXPath("//span[@data-text='Déposer une annonce']").catch(catchFunc))) {
          printLoading(2, "Waiting for Captcha resolve")
          await page.waitForXPath("//span[@data-text='Déposer une annonce']")
        }

        try {
          printLoading(3, "Getting address, title, date, ...")
          dataLine["title"] = await (await page.$x("//h1[@data-qa-id='adview_title']"))[0].evaluate((elem) => elem.innerText)
          dataLine["date"] = await (await page.$x("//p[@data-qa-id='adview_date']"))[0].evaluate((elem) => elem.innerText)
          dataLine["price"] = await (await page.$$("span._3gP8T._25LNb._35DXM"))[0].evaluate((elem) => elem.innerText)
          const address = (await (await page.$(`
          div._3JOrc._1akwr._2liDR._1mwQl.aj3_W.FB92D._3qvnf.PYBnx.rvrtO._23jKN._2hbpZ._1_1QY._1qvhT
          h2.Roh2X._3c6yv._25dUs._21rqc._3QJkO._1hnil._1-TTU._35DXM`)).evaluate((elem) => elem.innerText)).split("\n")
          if (address.length == 2) {
            dataLine["district"] = address[0]
            dataLine["address"] = address[1]
          } else {
            dataLine["address"] = address[0]
          }
          dataLine["isALaUne"] = Boolean(await page.$("div[data-tip='À la une']", { timeout: 500 }).catch(() => null))
          dataLine["isRemonteEnTete"] = Boolean(await page.$("div[data-tip='Remontée en tête de liste']", { timeout: 500 }).catch(() => null))
          dataLine["isUrgent"] = Boolean(await page.$("div[data-tip='Urgent']", { timeout: 500 }).catch(() => null))
          dataLine["isPro"] = Boolean(await page.$("svg.css-1hyucnh", { timeout: 500 }).catch(() => null) || await page.$("span._1mwQl.aj3_W._3kYlW.SWpW9._30glL._3k87M._387IP._16ME2._35DXM", { timeout: 500 }).catch(() => null))

          printLoading(4, "First Waiting")
          page.waitForTimeout(2000)

          printLoading(5, "Getting button 'Voir plus'")
          const button_more = await page.$x("//button[@data-qa-id='criteria_more']")
          if (button_more.length) {
            doWhile = await button_more[0].click().then(() => false).catch(() => true) || doWhile
          } else {
            await page.waitForNavigation().catch(() => true)
            while (!(await page.waitForXPath("//span[@data-text='Déposer une annonce']").catch(catchFunc))) {
              printLoading(2, "Waiting for Captcha resolve")
              await page.waitForXPath("//span[@data-text='Déposer une annonce']")
            }
          }

          if (doWhile) {
            continue
          }

          printLoading(6, "Finding criteria")
          const criteria_div = await page.$x("//div[@data-qa-id='criteria_container']/div")

          for (let i = 0; i < criteria_div.length; i++) {
            printLoading(7, "Getting attribut")
            let attr = ""
            title = await criteria_div[i].$$("p")
            if (title.length) {
              attr = await title[0].evaluate((elem) => elem.innerText)
            } else {
              attr = (await ((await criteria_div[i].$("._2k43C._1pHkp._21rqc._3QJkO.Dqdzf.cJtdT._3j0OU")).evaluate((elem) => elem.innerText))).split('\n', 1)[0]
            }
            if (attr === "") {
              doWhile = true
              break
            }

            printLoading(8, "Getting value")
            value = await criteria_div[i].$$("span")
            if (value.length) {
              dataLine[attr] = await value[0].evaluate((elem) => elem.innerText)
            } else {
              dataLine[attr] = await (await criteria_div[i].$("div.styles_active__j3Tua")).evaluate((elem) => elem.innerText)
            }
            if (dataLine[attr] === "") {
              doWhile = true
              break
            }
          }
          if (!button_more && !(await page.waitForXPath("//span[@data-text='Déposer une annonce']").catch(catchFunc))) {
            doWhile = true
          }
        } catch (error) {
          throw error
        }
      }
      printLoading(10, "Final sleep")
      // await page.waitForTimeout(1500)
      dataTable.push(dataLine)
      console.log(dataLine)
    }
    console.log(dataTable)

    if (OUTPUT_TYPE === "csv" || OUTPUT_TYPE === "both") {
      const csv = new ObjectsToCsv(dataTable);
      await csv.toDisk(`./output/${OUTPUT_NAME}.csv`, { allColumns: true });
    }
    if (OUTPUT_TYPE !== "csv") {
      fs.writeFile(`./output/${OUTPUT_NAME}.json`, JSON.stringify(dataTable, null, 4), 'utf8', (error) => {
        if (error) {
          console.log(error)
          console.log(dataTable)
        }
      })
    }
  }

  const html = await page.content();
  await browser.close();
}

const getCaptcha = async (page, url) => {
  await page.goto(url, { waitUntil: "load", timeout: 60000 }).catch(() => console.log("Une minute s'est écoulée et la page n'est toujours pas affiché :/"))
  let addAnnonceButton = await page.waitForXPath("//span[@data-text='Déposer une annonce']").catch(catchFunc)
  while (!addAnnonceButton) {
    console.log("Waiting for captchas...");
    addAnnonceButton = await page.waitForXPath("//span[@data-text='Déposer une annonce']").catch(catchFunc)
  }
}

const printLoading = (outOfTen, action) => {
  readline.moveCursor(process.stdout, 0, -1)
  readline.clearLine(process.stdout, 0)
  console.log(`[${"█".repeat(outOfTen) + " ".repeat(10 - outOfTen)}] - ${action}`)
}

const catchFunc = (error) => null

puppeteer.launch({ headless: false }).then(main)